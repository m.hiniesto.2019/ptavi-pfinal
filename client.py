

import sys
import socket
import socketserver
import time
import threading
from bitstring import BitArray

fecha = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
ip_c = '127.0.0.1'
port_c = 34543
def getArgv():  #funcion para obtener los argumento dados por el usuario
    # valid_methods = ('INVITE', 'BYE', 'ACK')
    
    if len(sys.argv) != 6:
        sys.exit(
            'Usage: python3 client.py <IPServidorSIP>:<puertoServidorSIP> <dirCliente> <dirServidorRTP> <tiempo> '
            '<fichero>')
    try:
        
        serverSIP = sys.argv[1]
        IPServidorSIP = str(serverSIP.split(':')[0])
        puertoServidorSIP = int(serverSIP.split(':')[1])
        dirCliente = sys.argv[2]
        protocolCliente, uriCliente = dirCliente.split(':')
        dirServidorRTP = sys.argv[3]
        protocolServidor, uriServidorRTP = dirServidorRTP.split(':')
        tiempo = int(sys.argv[4])
        fichero = sys.argv[5]
    except AttributeError:
        sys.exit(
            'Usage: python3 client.py <IPServidorSIP>:<puertoServidorSIP> <dirCliente> <dirServidorRTP> <tiempo> '
            '<fichero>')
    
    return IPServidorSIP, puertoServidorSIP, protocolCliente, uriCliente, protocolServidor, uriServidorRTP, tiempo, fichero


def createRequest():  # Creamos los cuatro metodos validos para la comunicacion SIP
    IPServidorSIP, puertoServidorSIP, protocolCliente, uriCliente, protocolServidor, uriServidorRTP, tiempo, fichero = getArgv()
    nombre, direccion = uriCliente.split('@')
    name, entidad = uriServidorRTP.split('@')
    inviteMethod = f'INVITE {protocolServidor}:{uriServidorRTP} SIP/2.0\nContent-Type: ' \
                   f'application/sdp\n\nv=0 ' \
                   f'\no=sip:{uriCliente} 127.0.0.1' \
                   f' \ns={name}Session \nt=0 \nm=audio 34543 RTP'
    byeMethod = f'BYE {protocolServidor}:{nombre}@{IPServidorSIP}:{puertoServidorSIP} SIP/2.0\r\n\r\n'
    
    ackMethod = f'ACK {protocolServidor}:{nombre}@{IPServidorSIP}:{puertoServidorSIP} SIP/2.0\r\n\r\n'
    
    return inviteMethod, byeMethod, ackMethod

class RTPHandler(socketserver.BaseRequestHandler):
    """Clase para manejar los paquetes RTP que nos lleguen.

    Atención: Heredamos de BaseRequestHandler porque UDPRequestHandler
    siempre termina enviando una respuesta al cliente, algo que aquí no
    queremos hacer. Al usar BaseRequestHandler no podemos usar self.rfile
    (porque esta clase no lo tiene), así que leemos el mensaje directamente
    de self.request (que es una lista, cuyo primer elemento es el mensaje
    recibido)"""

    def handle(self):
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)
        print("Received")

    @classmethod
    def open_output(cls, filename):
        """Abrir el fichero donde se va a escribir lo recibido.

        Lo abrimos en modo escritura (w) y binario (b)
        Es un método de la clase para que podamos invocarlo sobre la clase
        antes de empezar a recibir paquetes."""

        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido.

        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""
        cls.output.close()


def comunicacion_RTP(fichero, tiempo):
    RTPHandler.open_output(fichero)
    with socketserver.UDPServer((ip_c, port_c), RTPHandler) as serv:
        print("Listening...")
        threading.Thread(target=serv.serve_forever).start()
        time.sleep(tiempo)
        print("Time passed, shutting down receiver loop.")
        serv.shutdown()
    RTPHandler.close_output()

def main():
    IPServidorSIP, puertoServidorSIP, protocolCliente, uriCliente, protocolServidor, uriServidorRTP, tiempo, fichero = getArgv()
    nombre, direccion = uriCliente.split('@')
    name, entidad = uriServidorRTP.split('@')
    inviteMethod, byeMethod, ackMethod = createRequest()
    code_valids = ["200", "100", "180"]
    fin = False
    # Creamos el socket de envío, enviamos el mensaje de registro,
    # y esperamos respuesta.
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            # my_socket.connect((ip_sip, port_sip))
            print(f"{fecha} Starting...\n")
    
            # INVITE
            msj = inviteMethod
            my_socket.sendto(msj.encode('utf-8'), (IPServidorSIP, puertoServidorSIP))
            print(f"{fecha} SIP to {IPServidorSIP}:{puertoServidorSIP} {msj}")
            fin = False
            while not fin:
                # Manejo del mensaje del servidor
                data = my_socket.recv(1024).decode('utf-8')
                metodo = data.split()[1]
                print(f"{fecha} SIP from {IPServidorSIP}:{puertoServidorSIP} {data}")
                # respuesta al mensaje
                if metodo == "302":
                    # print(data)
                    msj = ackMethod
                    my_socket.sendto(msj.encode('utf-8'), (IPServidorSIP, puertoServidorSIP))
                    print(f"{fecha} SIP to {IPServidorSIP}:{puertoServidorSIP} {msj}")
                    # Envio a Servidor RTP
                    dir_rtp = data.split('\r\n\r\n')[1]
                    ip_rtp = dir_rtp.split("@")[0]
                    port_rtp = int(dir_rtp.split("@")[1])
                    print('hola')
                    # Mensaje para RTP INVITE
                    msj = f'INVITE {protocolServidor}:{name}@{ip_rtp}:{port_rtp} SIP/2.0\nContent-Type: ' \
                          f'application/sdp\n\nv=0 ' \
                          f'\no=sip:{uriCliente} 127.0.0.1' \
                          f' \ns={name}Session \nt=0 \nm=audio 34543 RTP'
                    # my_socket.connect((ip_rtp, port_rtp))
                    my_socket.sendto(msj.encode('utf-8'), (ip_rtp, port_rtp))
                    print(f"{fecha} SIP to {IPServidorSIP}:{puertoServidorSIP} {msj}")
                elif metodo in code_valids:
                    print(f"{data}\n")
                
                    if metodo == "200":
                        response = ackMethod
                        my_socket.sendto(response.encode('utf-8'), (ip_rtp, port_rtp))
                        print(f"{fecha} RTP ready.")
                    
                        # Envio de rtp y cliente en espera
                        comunicacion_RTP(fichero, tiempo)
                    
                        response = f'BYE {protocolServidor}:{nombre}@{ip_rtp}:{port_rtp} SIP/2.0\r\n\r\n'
                        fin = data.split('\n')[0]
                        my_socket.sendto(response.encode('utf-8'), (ip_rtp, port_rtp))
                        if metodo == "200":
                            print(f"{fecha}  {fin}")
                            print(f"{fecha} Ending...")
                            fin = True

    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == '__main__':
     main()
 