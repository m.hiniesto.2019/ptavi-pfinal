import socket
import socketserver
import sys
import simplertp
import time
import threading

# Constantes puerto.

fecha = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time())) # Pongo a aqui la fecha para que sea accesible desde
# todas las funciones.
port = 6002


def getargv():  #funcion para obtener los argumento dados por el usuario
    if len(sys.argv) != 4:
        sys.exit('Usage: python3 serverert.py <IPServidorSIP>:<puertoServidorSIP> <servicio> <fichero>')
    
    try:
        
        IPServidorSIP = str(sys.argv[1].split(':')[0])
        puertoServidorSIP = int(sys.argv[1].split(':')[1])
        servicio = sys.argv[2]
        fichero = sys.argv[3]
    
    except AttributeError:
        sys.exit('Usage: python3 serverert.py <IPServidorSIP>:<puertoServidorSIP> <servicio> <fichero>')
    
    return IPServidorSIP, puertoServidorSIP, servicio, fichero

class EchoHandler(socketserver.BaseRequestHandler):
    """
    Echo server class
    """
    
    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        valid_methods = ['INVITE', 'ACK', 'BYE']
        data = self.request[0]
        sock = self.request[1]
        received = data.decode('utf-8')
        metodo = received.split()[0]
        print(f'{fecha} SIP from {self.client_address[0]}:{self.client_address[1]} {received}')
        if metodo == valid_methods[0]:
            # print(self.client_address)
            global port_client
            global ip_client
            port_client = received.split("m=audio ")[1].split()[0]
            ip_client = received.split()[1].split(":")[1].split('@')[1]
            cuerpo = "\n".join(received.split("\n")[1:])
            response = 'SIP/2.0 200 OK\n' + cuerpo
            sock.sendto(response.encode('utf-8'), self.client_address)

        elif type(received) is not str:
            response = 'SIP/2.0 400 Bad Request'
            sock.sendto(response.encode('utf-8'), self.client_address)

        elif metodo not in valid_methods:
            response = 'SIP/2.0 405 Method Not Allowed'
            sock.sendto(response.encode('utf-8'), self.client_address)
            # Para BYE
        elif metodo == valid_methods[2]:
            ok = 'SIP/2.0 200 OK'
            sock.sendto(ok.encode('utf-8'), self.client_address)
    
            # Para ACK
        elif metodo == valid_methods[1]:
            audio_file = sys.argv[3]
            print(f"{fecha} RTP to {ip_client}:{port_client}")
            envio_RTP = simplertp.RTPSender(ip='127.0.0.1', port=34543, file=audio_file, printout=True)
            envio_RTP.send_threaded()
            time.sleep(2)
            print("Finalizando el thread de envío.")
            envio_RTP.finish()
        elif metodo == "200":
            pass
def registrar(usuario):
    request = f'REGISTER sip:{usuario}@songs.net SIP/2.0\r\nExpire: 62\r\n\r\n'
    return request

def main():
    IPServidorSIP, puertoServidorSIP, servicio, fichero = getargv()
    
    def timer():
        while True:
            msj = registrar(servicio)
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                my_socket.sendto(msj.encode('utf-8'), (IPServidorSIP, puertoServidorSIP))
                print(f"{fecha} SIP to {IPServidorSIP}:{puertoServidorSIP} {msj}")
                time.sleep(30)
    try:
        register_return = threading.Thread(target=timer)
        register_return.start()
    except:
        sys.exit('Error: No se ha podido registrar en el servidor')
    try:
        serv = socketserver.UDPServer(('', port), EchoHandler)
        print(f"{fecha} Listening...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
