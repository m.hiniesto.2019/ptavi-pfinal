import socketserver
import sys
import json
import time

# Constantes Puerto.


fecha = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))  # Pongo a aqui la fecha para que sea accesible desde
# todas las funciones.




def getargv():  # funcion para obtener los argumento dados por el usuario
    if len(sys.argv) != 2:
        sys.exit('Usage: python3 serverSIP.py <port>')

    try:
        port = int(sys.argv[1])

    except AttributeError:
        sys.exit('Usage: python3 serverSIP.py <port>')

    return port


class EchoHandler(socketserver.BaseRequestHandler):
    """
    Echo server class
    """
    registered = {}
    
    def json2registered(self):

        try:
            with open('registered.json', 'r') as file:
                self.registered = json.load(file)
        except BaseException as exp:
            print('Error: no se pudo leer el archivo registered.json')
            print(f' (Exception: {exp})')
            self.registered = {}
    
    def registered2json(self):
        with open('registered.json', 'w') as file:
            json.dump(self.registered, file, indent=4)
    
    def register(self, dir, expires):
        expire_time = time.time() + expires
        expire_string = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(expire_time))
        if expires == 0:
            if dir in self.registered:
                del self.registered[dir]
        elif dir in self.registered:
            del self.registered[dir]
            self.registered[dir] = (self.client_address[0], self.client_address[1], f"{expire_string} + {expires}")
        else:
            self.registered[dir] = (self.client_address[0], self.client_address[1], f"{expire_string} + {expires}")
        self.registered2json()
    
    def expired(self):
        list_clients = []
        time_now = time.time()
        for dir in self.registered:
            if self.registered[dir][2] <= time_now:
                list_clients.append(dir)
    
    def buscar_rtp(self, direccion):
        try:
            return self.registered[direccion]
        except:
            return "Not found"
    
    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.json2registered()
        metodos_validos = ("REGISTER", "INVITE", "ACK", "BYE")
        data = self.request[0]
        sock = self.request[1]
        received = data.decode('utf-8')
        metodo = received.split()[0]
        dir = received.split()[1]
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        
        if type(received) is not str:
            response = 'SIP/2.0 400 Bad Request'
            sock.sendto(response.encode('utf-8'), self.client_address)
        
        elif metodo not in metodos_validos:
            print(f"{fecha} SIP from {self.client_address[0]}:{self.client_address[1]} {received}")
            response = 'SIP/2.0 405 Method Not Allowed'
            sock.sendto(response.encode('utf-8'), self.client_address)
        
        # REGISTER
        elif metodo == metodos_validos[0]:
            expires = int(received.split()[4])
            response = 'SIP/2.0 200 OK\n'
            sock.sendto(response.encode('utf-8'), self.client_address)
            print(f"{fecha} SIP from {client_ip}:{str(client_port)}: {metodo}: {dir}")
            if dir not in self.registered:
                self.register(dir, expires)
        
        # INVITE
        elif metodo == metodos_validos[1]:
            print(f"{fecha} SIP from {client_ip}:{str(client_port)}: {metodo}: {dir}")
            destino = received.split()[1]
            result_rtp = self.buscar_rtp(destino)
            if result_rtp == "Not found":
                response = "SIP/2.0 404 Not Found\r\n\r\n"
                sock.sendto(response.encode('utf-8'), self.client_address)
            else:
                ip_rtp = self.registered[dir][0]
                port_rtp = self.registered[dir][1]
                msj = "SIP/2.0 302 Move Temporality\r\n\r\n"
                response = msj + str(ip_rtp) + "@" + str(port_rtp)
                sock.sendto(response.encode('utf-8'), self.client_address)
        
        # ACK
        elif metodo == metodos_validos[2]:
            print(f"{fecha} SIP from {client_ip}:{str(client_port)} {metodo}:{dir}")


def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    port = getargv()
    try:
        serv = socketserver.UDPServer(('', port), EchoHandler)
        print(f"{fecha} Listening...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")
    
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
